// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

/* 
	Randomization support
	
	--- Sub-class randomization --- 
	
	Any itemCargos class can define items through a sub-class instead of in the main class.
	In this case, the script will pick one of the sub-classes at random to apply to the object.
	The names of the sub-classes do not matter.
	
	The following example will select one of "box1" or "box2" when the "MyAmmoBox" class is called.
	The names of the subclasses don't matter, and they can be named whatever you want them to be.
	
	class itemCargos
	{
		class MyAmmoBox
		{
			class box1
			{
				items[] = {};
			};
			class box2
			{
				items[] = {};
			};
		};
	};
	
	
	--- Per-item randomization ---
	
	Any item can have the quantity changed from a fixed value, to a randomized value.
	By creating an array of three numbers instead of a single one, the itemCargo script will randomly select a quantity using the provided numbers.
	
	Example:
	
	class MyAmmoBox
	{
		items[] = {"FirstAidKit", {1,3,5}}; // Spawns a minimum of 1, a maximum of 5, and an average of 3
	};

*/
class itemCargos
{
	class example
	{
		// Array containing sub-arrays of items to add
		// Sub-arrays must include an item classname, and a quantity
		// The following would add 5 first aid kits to the inventory of the object
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE basic medical is being used
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE advanced medical is being used
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	
	class supplybox
	{
		items[] = {
			{"ACE_HandFlare_Green", 16},
			{"ACE_HandFlare_Red", 16},
			{"CUP_7Rnd_45ACP_1911", 8},
			{"CUP_Dragon_EP1_M", 3},
			{"MiniGrenade", 8},
			{"SmokeShell",12},
			{"1Rnd_HE_Grenade_shell", 10},
			{"CUP_1Rnd_StarFlare_White_M203", 4},
			{"CUP_1Rnd_StarFlare_Green_M203", 4},
			{"CUP_1Rnd_StarFlare_Red_M203", 4},
			{"hlc_30Rnd_9x19_B_MP5", 16},
			{"CUP_20Rnd_556x45_Stanag_Tracer_Red", 60},
			{"ACE_20Rnd_762x51_Mag_Tracer", 12},
			{"hlc_100Rnd_762x51_T_M60E4", 8},
			{"ACE_EntrenchingTool", 8}
		};
		itemsBasicMed[] = {
			{"ACE_quikclot", 100},
			{"ACE_bloodIV", 20},
			{"ACE_epinephrine", 40},
			{"ACE_morphine", 10},
			{"ACE_adenosine", 24}
		};
		itemsAdvMed[] = {};
	};
	
	class m113
	{
		items[] = {
			{"ACE_HandFlare_Green", 6},
			{"ACE_HandFlare_Red", 6},
			{"CUP_7Rnd_45ACP_1911", 8},
			{"CUP_Dragon_EP1_M", 1},
			{"MiniGrenade", 4},
			{"1Rnd_HE_Grenade_shell", 4},
			{"CUP_1Rnd_StarFlare_White_M203", 2},
			{"CUP_1Rnd_StarFlare_Green_M203", 2},
			{"CUP_1Rnd_StarFlare_Red_M203", 2},
			{"hlc_30Rnd_9x19_B_MP5", 16},
			{"CUP_20Rnd_556x45_Stanag_Tracer_Red", 16},
			{"ACE_20Rnd_762x51_Mag_Tracer", 4},
			{"hlc_100Rnd_762x51_T_M60E4", 4},
			{"ACE_EntrenchingTool", 5}
		};
		itemsBasicMed[] = {
			{"ACE_quikclot", 20},
			{"ACE_bloodIV", 2},
			{"ACE_epinephrine", 16},
			{"ACE_morphine", 8},
			{"ACE_adenosine", 16}
		};
		itemsAdvMed[] = {};
	};
	
	class helicopter
	{
		items[] = {};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
};
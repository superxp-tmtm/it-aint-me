// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"CUP_hgun_Colt1911","","","",{"CUP_7Rnd_45ACP_1911",7},{},""};
		binocular = "Binocular";
		
		uniformClass = "U_B_JaKdo_Oliv_sleeve";
		headgearClass = "CUP_H_USArmy_Helmet_M1_Olive";
		facewearClass = "";
		vestClass = "V_TacVest_oli";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemMicroDAGR", "TFAR_rf7800str", "ItemCompass", "ItemWatch", ""};
		
		uniformItems[] = {{"ItemcTabHCam",1},{"ACE_HandFlare_Green",2,1},{"ACE_HandFlare_Red",1,1},{"SmokeShell",1,1}};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_morphine",2},{"ACE_adenosine",2},{"ACE_epinephrine",2}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Officer_F: base
	{
		displayName = "Commander";
		
		primaryWeapon[] = {"CUP_arifle_M16A1GL_USA","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{"1Rnd_HE_Grenade_shell",1},""};

		headgearClass = "H_MilCap_gry";
		facewearClass = "G_Aviator";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green"; // Maybe switch to Contact radio bag

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_rf7800str","ItemCompass","ItemWatch",""};

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",9,20},{"MiniGrenade",1,1}};
		backpackItems[] = {{"CUP_1Rnd_StarFlare_Green_M203",4,1},{"CUP_1Rnd_StarFlare_Red_M203",4,1},{"CUP_1Rnd_StarFlare_White_M203",4,1},{"1Rnd_HE_Grenade_shell",10,1}};
	};
	
	class cmd_medic: base
	{
		displayName = "Command Medic";
		
		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};

		headgearClass = "CUP_H_USArmy_Helmet_M1_Olive";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_rf7800str","ItemCompass","ItemWatch",""};

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",9,20},{"MiniGrenade",1,1}};
		backpackItems[] = {};

		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_fieldDressing",75},{"ACE_epinephrine",15},{"ACE_morphine",10},{"ACE_adenosine",10},{"ACE_bloodIV",5}};
	};
	
	class B_Soldier_F: base
	{
		displayName = "Rifleman";

		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"MiniGrenade",1,1},{"hlc_100Rnd_762x51_T_M60E4",1,100}};

		basicMedVest[] = {};
	};
	
	class B_Soldier_SL_F: base
	{
		displayName = "Squad Leader";

		primaryWeapon[] = {"CUP_arifle_M16A1GL","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{"1Rnd_HE_Grenade_shell",1},""};

		facewearClass = "G_Aviator";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_rf7800str","ItemCompass","ItemWatch",""};
		
		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"MiniGrenade",1,1},{"hlc_100Rnd_762x51_T_M60E4",1,100}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",10,1},{"CUP_1Rnd_StarFlare_Green_M203",4,1},{"CUP_1Rnd_StarFlare_Red_M203",4,1},{"CUP_1Rnd_StarFlare_White_M203",4,1}};
	};
	
	class B_Soldier_AT_F: base
	{
		displayName = "Anti-Tank Specialist";

		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};
		secondaryWeapon[] = {"CUP_launch_M47","","","",{"CUP_Dragon_EP1_M",1},{},""};

		backpackClass = "B_Carryall_oli";

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"hlc_100Rnd_762x51_T_M60E4",1,100},{"MiniGrenade",1,1}};
		backpackItems[] = {{"CUP_Dragon_EP1_M",1,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_Soldier_AAT_F: base
	{
		displayName = "Assistant Anti-Tank Specialist";
		
		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};

		backpackClass = "B_Carryall_oli";

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"hlc_100Rnd_762x51_T_M60E4",1,100},{"MiniGrenade",1,1}};
		backpackItems[] = {{"CUP_Dragon_EP1_M",1,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_Soldier_A_F: base
	{
		displayName = "Ammo Bearer";

		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};

		backpackClass = "B_Carryall_oli";

		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"MiniGrenade",1,1},{"hlc_100Rnd_762x51_T_M60E4",1,100}};
		backpackItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",30,20}, {"ACE_20Rnd_762x51_Mag_Tracer",5,20}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	}; 
	
	class B_Medic_F: base
	{
		displayName = "Medic";
		
		primaryWeapon[] = {"CUP_arifle_M16A1","","","",{"CUP_20Rnd_556x45_Stanag_Tracer_Red",20},{},""};

		backpackClass = "B_Carryall_oli";
		
		vestItems[] = {{"CUP_20Rnd_556x45_Stanag_Tracer_Red",5,20},{"MiniGrenade",1,1},{"hlc_100Rnd_762x51_T_M60E4",1,100}};
		backpackItems[] = {};

		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",15},{"ACE_morphine",10},{"ACE_adenosine",15},{"ACE_bloodIV",5}};
	};
	
	class B_Soldier_M_F: base
	{
		displayName = "Marksman";
		
		primaryWeapon[] = {"srifle_DMR_06_hunter_F","","","optic_DMS_weathered_F",{"ACE_20Rnd_762x51_Mag_Tracer",20},{},"bipod_01_F_blk"};

		vestItems[] = {{"MiniGrenade",1,1},{"ACE_20Rnd_762x51_Mag_Tracer",7,20}};

		basicMedVest[] = {};
	};
	
	class B_Soldier_AR_F: base
	{
		displayName = "Autorifleman";

		primaryWeapon[] = {"CUP_lmg_M60","","","",{"hlc_100Rnd_762x51_T_M60E4",100},{},""};

		vestItems[] = {{"hlc_100Rnd_762x51_T_M60E4",1,100},{"MiniGrenade",1,1}};
		backpackItems[] = {{"hlc_100Rnd_762x51_T_M60E4",2,100}};

		basicMedVest[] = {};
	};
	
	class B_Pilot_F: base
	{
		displayName = "Pilot";
		
		primaryWeapon[] = {"hlc_smg_mp5a3","","hlc_acc_Surefiregrip","",{"hlc_30Rnd_9x19_B_MP5",30},{},""};
		handgunWeapon[] = {"hgun_Pistol_Signal_F","","","",{"6Rnd_RedSignal_F",6},{},""};
		binocular = "Binocular";

		uniformClass = "U_B_HeliPilotCoveralls";
		headgearClass = "CUP_H_USMC_Helmet_Pilot";
		facewearClass = "G_Aviator";
		vestClass = "V_TacVest_oli";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_rf7800str","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"6Rnd_GreenSignal_F",1,6}};
		vestItems[] = {{"hlc_30Rnd_9x19_B_MP5",6,30},{"MiniGrenade",1,1},{"ACE_HandFlare_Green",2,1},{"ACE_HandFlare_Red",2,1}};
		backpackItems[] = {{"ToolKit",1}};

		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_adenosine",2}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_Engineer_F: base
	{
		displayName = "Combat Engineer";
		
		primaryWeapon[] = {"hlc_smg_mp5a2","","hlc_acc_Surefiregrip","",{"hlc_30Rnd_9x19_B_MP5",30},{},""};

		backpackClass = "B_AssaultPack_rgr";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_rf7800str","ItemCompass","ItemWatch",""};

		vestItems[] = {{"ACE_EntrenchingTool",1},{"ACE_SpraypaintGreen",1},{"hlc_30Rnd_9x19_B_MP5",6,30},{"MiniGrenade",1,1}};
		backpackItems[] = {{"ToolKit",1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class cmd_engineer: B_Engineer_F
	{
		displayName = "Combat Engineer Leader";
		
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
		
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_rf7800str","ItemCompass","ItemWatch",""};
	};
};
// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
[] execVM "scripts\briefing.sqf";

_zeus = _player getVariable ["XPT_zeusUnit", false];
// Check if the player is marked as a zeus unit
if !(_zeus isEqualTo false) then {
	// Disable ambient sound (i.e. wind noises) for zeus
	enableEnvironment [true, false];
};
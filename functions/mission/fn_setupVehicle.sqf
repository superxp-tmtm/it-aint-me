// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// M113s
	case (toLower "CUP_B_M163_USA");
	case (toLower "CUP_B_M113_USA"): {
		// Set the fuel, kill the engine, and damage the hull
		_newVeh setFuel (random [0.5, 0.7, 0.9]);
		_newVeh setHitPointDamage ["HitEngine", 1];
		_newVeh setHitPointDamage ["HitHull", random [0.3, 0.5, 0.7]];
		[_newVeh, "m113"] call XPT_fnc_loadItemCargo;
	};
	
	// Logistics Helicopters
	case (toLower "CUP_B_UH1D_slick_GER_KSK_Des"): {
		[_newVeh,"helicopter"] call XPT_fnc_loadItemCargo;
	};
};